---
title: Henry berichtet über Joplin
---

![Stierkopf](images/stierkopf.png)

# Henry arbeitet mit Joplin

## MarkDown

[home](https://henry.freye.de)

## Zusammenspiel mit Next-Cloud

## Tipps und Tricks

## offene Fragen - Schattenseiten


# Unterrichten mit #AnnasLeben
# Vorbemerkung:
Der Plan war im OER-Camp ein Material zu [annasleben.de](https://www.annasleben.de/) zu entwickeln, dass später in [RLP-Online](https://bildungsserver.berlin-brandenburg.de/rlp-online/) ([Materialübersicht](https://bildungsserver.berlin-brandenburg.de/themen/medienbildung/unterrichten/unterrichtsmaterialien/)) veröffentlicht werden soll. Davor muss es aber einen Publikationsprozess im Haus durchlaufen.
Wie zu erwarten, habe ich mir zu viel vorgenomen. Eigene Inputs und Support konkurierten um die Zeit. Das Produkt hat "verloren". Aber ich MUSS es fertig stellen, darum geht es hier demnächst weiter.
Feedback erwünscht an @freye henry.freye@lisum.berlin-brandenburg.de

Alle Ergebnisse der OER-Camps/Werkstätten sind online sichtbar unter: https://oercamp.de/werstatt/ergebnisse
----

# Annas Leben
Henry Freye, [Landesinstitut für Schule und Medien Berlin-Brandenburg (LISUM)](https://lisum.berlin-brandenburg.de/lisum/)

Die Webseite [Annas Leben](https://annasleben.de) bietet zu aktuellen Digitalisierungsthemen Videos, Podcasts und Hintergrundwissen, die sich gut im Unterricht einsetzen lassen. Hier eine Unterrichtsideen dazu.

# AUF EINEN BLICK
Jahrgangsstufe 9, Niveaustufe F/G

### Fach (fachübergreifende Bezüge)
Politische Bildung
Themen und Inhalte
[aus den Themenfeldern im Fach]

Kompetenzbereich(e) im Fach
[...]


Wirtschaft-Arbeit-Technik
Themen und Inhalte
[aus den Themenfeldern im Fach]

Kompetenzbereich(e) im Fach
[...]

## Kompetenzbereich(e) im Basiscurriculum Medienbildung
+ Reflektieren (Eigener Mediengebrauch,Medien in Politik und Gesellschaft)
+ Analysieren (Bedeutung und Wirkung von Medienangeboten)

## Zeitbedarf
Doppelstunde (nach oben offen)

## Materialien
https://annasleben.de


# HINWEISE 
[möglich ist hier ein breites Spektrum an Wahlmöglichkeiten oder Kombinationen, z.B.: didaktischer Kommentar mit Verbindung zu den erwähnten Standards, vorbereitende Hinweise, Kontextualisierung, geschichtlicher, politischer, künstlerischer etc. Hintergrund, Tipps & Tools etc.]
Die Webseite [annasleben.de](https://www.annasleben.de/) liefert auch zwei Anwendungsideen in einer [Handreichung](https://www.annasleben.de/wp-content/uploads/2019/03/ANNA_Handreichung_Bildungsarbeit.pdf).
Die hier beschriebenen Ideen folgen dem fliiped clasroom Ansatz. Die Schüler\*innen sehen sich zu Hause die Seite annasleben.de an und arbeiten sich selbst ins Thema ein. Im Unterricht wird dann ausgewertet und vertiefend gearbeitet. Eine andere grunzsätzliche Überlegung ist der wechsel von Perspektiven, um die verschiedenen Interessen besser zu verstehen. 

# BAUSTEINE FÜR DEN UNTERRICHT
Thema / Schwerpunkt
Methode und Inhalt
Materialien und Tipps
### [Thema/Schwerpunkt]
    • XXX 
    • XXX 
### [Thema/Schwerpunkt]
    • XXX
    • XXX 
### [Thema/Schwerpunkt]
    • XXX
    • XXX 

...

# Literatur, Links und EMPFEHLUNGEN

+ ["Smarte schöne neue Welt?](http://www.bpb.de/gesellschaft/medien-und-sport/medienpolitik/236524/internet-der-dinge?p=all), bpb, 2016
+ [big data und politische Bildung](http://www.bpb.de/lernen/big-data), bpb
+ [Smart Data - Innovationen aus Daten](https://www.bmwi.de/Redaktion/DE/Artikel/Digitale-Welt/smart-data.html), BMWi
+ 

# Informationen zu den unterrichtsbausteinen 
• Begleitende Hinweisbroschüre: https://s.bsbb.eu/hinweise 
    • Unterrichtsbausteine für alle Fächer im Überblick: https://s.bsbb.eu/ueberblick 

[CC-BY-4.0](https://creativecommons.org/licenses/by/4.0/deed.de) Henry Freye


[Read documentation](https://middlemanapp.com/basics/templating_language/)